<html>
    <head>
    <style>
                    body{
                        margin:0;
                        background-color: #333333;
                        font-family: "open sans";
                        font-size: 14px;
                        color: #fff;
                        text-align: center;
                    }
                    
                    #wrapper{
                        background-color:#dca670;
                        opacity:0.7;
                        filter:alpha(opacity=70);
                        max-width: 960px;
                        margin:auto;
                        -webkit-box-flex: 1;
                    }
                  
                    pre{
                        font-family: "open sans";
                        font-size: 12px;
                    }
                    
                    #nav{
                        margin:0;
                        padding: 50px 0 50px 0;
                    }
                    
                    ul{
                        margin:0;
                    }
                    
                    li{
                        margin-left: 20px;
                        display:inline;
                    }
                    li:first-child{
                        margin:0;
                    }
                    
                    #nav li a{
                        background-color: #868686;
                        padding: 5px 15px;
                        text-decoration: none;
                        color:#fff;
                        -webkit-transition: all 0.3s ease-in-out;
                        border:1px solid #868686;
                        border-radius:14px;
                    }
                    
                    #nav li a:hover, #nav li a:focus{
                        background-color: #fff;
                        border-color: #fff;
                        color:#868686;
                    }
                    
                    input.rond{
                        border:1px solid black;
                        border-radius:8px;
                    }
    </style>
    </head>
    <body>
        <div id="wrapper">
            <div id="nav">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Over ons</a></li>
                    <li><a href="#">Producten</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </div>
            <?php
                $balance = 150;
                $value = 20;

                $newBalance = $balance + $value;
                ?>
                <h1>Storting</h1>
                <p>
                    <?php echo $value; ?> &euro; storten op een rekening met <?php echo $balance; ?> &euro; als saldo geeft een nieuw saldo van <?php echo $newBalance; ?>
                </p>

                <?php
                $balance = $newBalance;
                $value = 50;
                $newBalance = $balance - $value;
                ?>

                <h1>Afhaling</h1>
                <p>
                    <?php echo $value; ?> &euro; afhalen van een rekening met <?php echo $balance; ?> &euro; als saldo geeft een nieuw saldo van <?php echo $newBalance; ?>
                </p>
                        
                <form>
                    Gebruikersnaam: <input type="text" class="rond" />
                    Wachtwoord: <input type="password" class="rond" />
                    <br />
                    <input type="submit" value="stuur ke doo" />
                </form>
        
        </div>
    </body>
</html>